# Xen-Orchestra - Community Edition

[Xen-Orchestra](https://xen-orchestra.com/) is a cloud-ready turnkey solution for administering XenServer and XCP-ng.

## Introduction

This chart bootstraps an [Xen-Orchestra Docker Container](https://github.com/ronivay/xen-orchestra-docker) from [Roni Väyrynen](https://github.com/ronivay) deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

